﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFurnitureType", menuName = "Types/Furniture Type", order = 51)]
public class FurnitureType : ScriptableObject
{
	[SerializeField]
	private Vector3 _size = Vector3.one;

	[SerializeField]
	private Sprite _sprite = null;

	[SerializeField]
	private bool _isVertical = false;
	[SerializeField]
	private bool _isHorizontal = true;

	public string Name => name;

	public Vector3 Size => _size;

	public Sprite Sprite => _sprite;

	public bool IsVertical => _isVertical;
	public bool IsHorizontal => _isHorizontal;
}
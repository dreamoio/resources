﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFurniture", menuName = "Furniture/Furniture", order = 52), Serializable]
public class Furniture : ScriptableObject
{
	private static int lastId = 0;

	[SerializeField]
	private int _id;

	[SerializeField]
	private int _price;

	[SerializeField]
	private Sprite _sprite = null;

	public int Id => _id;

	public string Name => name;

	public int Price => _price;

	public Sprite Sprite => _sprite;

	private void Awake()
	{
		_id = ++lastId;
	}

	public string ToJson()
	{
		return JsonUtility.ToJson(this);
	}
}

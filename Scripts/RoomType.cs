﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "NewRoomType", menuName = "Types/Room Type", order = 51)]
public class RoomType : ScriptableObject
{
	public string Name => name;
}